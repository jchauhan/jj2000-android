/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000facade;
import jj2000.j2k.decoder.Decoder;
import jj2000.j2k.util.ParameterList;


public class JJ2000DecoderFacade {
	private Decoder dec;
	/** The parameter info, with all possible options. */
    private static String pinfo[][] = Decoder.getAllParameters();

    /** The parameter list to give to the decoder */
    private ParameterList pl;
	
  //Output file locations, in case PGM multiple files will be generated
  	String[] outputFilesPaths;
  	//Number of components. 
  	int numComponents;
  	
  	public String[] getOutputFilesPaths() {
  		return outputFilesPaths;
  	}


  	public int getNumComponents() {
  		return numComponents;
  	}
    
	public JJ2000DecoderFacade()
	{
	}
	
	public void decode( String inputFileName, String outputFileName )
	{
		ParameterList defpl;
		String param;
		int i;

		// Get the dfault parameter values
		defpl = new ParameterList();
		for (i=pinfo.length-1; i>=0; i--) {
			if(pinfo[i][3]!=null)
				defpl.put(pinfo[i][0],pinfo[i][3]);
		}

		// Get all parameters from and put them in a parameter list
		pl = new ParameterList(defpl);
		for (i=0; i<pinfo.length; i++) {
			param = getParameter(pinfo[i][0]);
			if (param != null) {
				pl.put(pinfo[i][0],param);
			}
		}
		
		pl.setProperty("rate", "3");
		pl.setProperty("o", outputFileName);
		pl.setProperty("debug", "on");
		
		// Add base to relative URL
		pl.setProperty("i", inputFileName );
		param = pl.getParameter("i");
		
		
        showStatus("Initializing JJ2000 decoder...");
        dec = new Decoder(pl);
        showStatus("Decoding...");
        dec.run();
        outputFilesPaths = dec.getOutputFilesPaths();
        numComponents = dec.getNumComponents();
	}
	

	private String getParameter(String string) {
		return null;
	}

	/**
     * Returns the applet parameter information. See Applet class.
     *
     * @see Applet#getParameterInfo
     * */
    public String[][] getParameterInfo() {
        return pinfo;
    }
    
	private void showStatus(String string) {
		System.out.println(string);
	}
	
}
