/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000facade;

import java.util.Map.Entry;

import jj2000.j2k.encoder.Encoder;
import jj2000.j2k.util.ParameterList;

public class JJ2000EncoderFacade {
	private Encoder dec;
	/** The parameter info, with all possible options. */
    private static String pinfo[][] = Encoder.getAllParameters();

    /** The parameter list to give to the decoder */
    private ParameterList pl;
	
	public JJ2000EncoderFacade()
	{
	}
	
	public void encode( String inputFileName, String outputFileName, boolean codeStreamOnly)
	{
		ParameterList defpl;
		String param;
		int i;

		// Get the dfault parameter values
		defpl = new ParameterList();
		for (i=pinfo.length-1; i>=0; i--) {
			if(pinfo[i][3]!=null)
				defpl.put(pinfo[i][0],pinfo[i][3]);
		}

		// Get all parameters from and put them in a parameter list
		pl = new ParameterList(defpl);
		for (i=0; i<pinfo.length; i++) {
			param = getParameter(pinfo[i][0]);
			if (param != null) {
				pl.put(pinfo[i][0],param);
			}
		}
		
		pl.setProperty("rate", "-1");
		pl.setProperty("o", outputFileName);
		pl.setProperty("debug", "on");

		pl.setProperty("cso", codeStreamOnly ? "on" : "false");

		
		pl.setProperty("file_format", "off");

		pl.setProperty("lossless", "on");

		// Add base to relative URL
		pl.setProperty("i", inputFileName );
		param = pl.getParameter("i");
		
		
        showStatus("Initializing JJ2000 encoder...");
       
        StringBuilder sb  = new StringBuilder();
        for(Entry<Object, Object> e: pl.entrySet())
        {
        	sb.append("-" + e.getKey() + " " + e.getValue() + " ");
        }
        
        
        dec = new Encoder(pl);
        showStatus("Encoding... parameters " + sb.toString());
        dec.run();
	}
	

	private String getParameter(String string) {
		return null;
	}

	/**
     * Returns the applet parameter information. See Applet class.
     *
     * @see Applet#getParameterInfo
     * */
    public String[][] getParameterInfo() {
        return pinfo;
    }
    
	private void showStatus(String string) {
		System.out.println(string);
	}
	
}
