/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package icc;

import icc .tags.ICCCurveType;

/**
 * This class is a 1 component RestrictedICCProfile
 * 
 * @version	1.0
 * @author	Bruce A Kern
 */
public class MonochromeInputRestrictedProfile extends RestrictedICCProfile {

    /**
     * Factory method which returns a 1 component RestrictedICCProfile
     *   @param c Gray TRC curve
     * @return the RestrictedICCProfile
     */
    public static RestrictedICCProfile createInstance (ICCCurveType c) {
        return new MonochromeInputRestrictedProfile(c);}

    /**
     * Construct a 1 component RestrictedICCProfile
     *   @param c Gray TRC curve
     */
    private MonochromeInputRestrictedProfile (ICCCurveType c) {
        super (c); }

    /**
     * Get the type of RestrictedICCProfile for this object
     * @return kMonochromeInput
     */
    public  int getType () {return kMonochromeInput;}

    /**
     * @return String representation of a MonochromeInputRestrictedProfile
     */
    public String toString () {
        StringBuffer rep = new StringBuffer ("Monochrome Input Restricted ICC profile" + eol);

        rep .append("trc[GRAY]:"   + eol).append(trc[GRAY]).append(eol);

        return rep.toString(); }

    /* end class MonochromeInputRestrictedProfile */ }








