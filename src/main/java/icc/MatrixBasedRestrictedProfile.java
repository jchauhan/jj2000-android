/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package icc;

import icc .tags.ICCCurveType;
import icc .tags.ICCXYZType;

/**
 * This class is a 3 component RestrictedICCProfile
 * 
 * @version	1.0
 * @author	Bruce A Kern
 */
public class MatrixBasedRestrictedProfile extends  RestrictedICCProfile { 

    /**
     * Factory method which returns a 3 component RestrictedICCProfile
     *   @param rcurve Red TRC curve
     *   @param gcurve Green TRC curve
     *   @param bcurve Blue TRC curve
     *   @param rcolorant Red colorant
     *   @param gcolorant Green colorant
     *   @param bcolorant Blue colorant
     * @return the RestrictedICCProfile
     */
    public static RestrictedICCProfile createInstance (ICCCurveType rcurve, ICCCurveType gcurve, ICCCurveType bcurve, 
                        ICCXYZType rcolorant, ICCXYZType gcolorant, ICCXYZType bcolorant) {
        return new MatrixBasedRestrictedProfile(rcurve,gcurve,bcurve,rcolorant,gcolorant,bcolorant); }
    
    /**
     * Construct a 3 component RestrictedICCProfile
     *   @param rcurve Red TRC curve
     *   @param gcurve Green TRC curve
     *   @param bcurve Blue TRC curve
     *   @param rcolorant Red colorant
     *   @param gcolorant Green colorant
     *   @param bcolorant Blue colorant
     */
    protected MatrixBasedRestrictedProfile (ICCCurveType rcurve, ICCCurveType gcurve, ICCCurveType bcurve, 
                        ICCXYZType rcolorant, ICCXYZType gcolorant, ICCXYZType bcolorant) {
        super (rcurve, gcurve, bcurve, rcolorant, gcolorant, bcolorant); }

    /**
     * Get the type of RestrictedICCProfile for this object
     * @return kThreeCompInput
     */
    public  int getType () {return kThreeCompInput;}

    /**
     * @return String representation of a MatrixBasedRestrictedProfile
     */
    public String toString () {
        StringBuffer rep = 
            new StringBuffer ("[Matrix-Based Input Restricted ICC profile").append(eol);

        rep .append("trc[RED]:").append(eol).append(trc[RED]).append(eol);
        rep .append("trc[RED]:").append(eol).append(trc[GREEN]).append(eol);
        rep .append("trc[RED]:").append(eol).append(trc[BLUE]).append(eol);

        rep .append("Red colorant:  ").append(colorant[RED]).append(eol);
        rep .append("Red colorant:  ").append(colorant[GREEN]).append(eol);
        rep .append("Red colorant:  ").append(colorant[BLUE]).append(eol);

        return rep.append("]").toString(); }

    /* end class MatrixBasedRestrictedProfile */ }



