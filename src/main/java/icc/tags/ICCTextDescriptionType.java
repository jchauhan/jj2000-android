/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package icc.tags;

import java.util.Vector;
import icc .ICCProfile;

/**
 * A text based ICC tag
 * 
 * @version	1.0
 * @author	Bruce A. Kern
 */
public class ICCTextDescriptionType extends ICCTag {

    /** Tag fields */ public final int type;
    /** Tag fields */ public final int reserved;
    /** Tag fields */ public final int size;
    /** Tag fields */ public final byte [] ascii;

    /**
     * Construct this tag from its constituant parts
     *   @param signature tag id
     *   @param data array of bytes
     *   @param offset to data in the data array
     *   @param length of data in the data array
     */
    protected ICCTextDescriptionType (int signature, byte [] data, int offset, int length) {

        super (signature, data, offset, length);

        type = ICCProfile.getInt (data, offset);
        offset += ICCProfile.int_size;

        reserved = ICCProfile.getInt (data, offset);
        offset += ICCProfile.int_size;

        size = ICCProfile.getInt (data, offset);
        offset += ICCProfile.int_size;

        ascii = new byte [size-1];
        System.arraycopy (data,offset,ascii,0,size-1); }

    /** Return the string rep of this tag. */
    public String toString () {
        return "[" + super.toString() + " \"" + new String (ascii) + "\"]"; }

    /* end class ICCTextDescriptionType */ }











