/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package icc.types;

import java.io.IOException;
import java.io.RandomAccessFile;
import icc .ICCProfile;

/**
 * A convientient representation for the contents of the
 * ICCXYZTypeTag class.
 * 
 * @see		jj2000.j2k.icc.tags.ICCXYZType
 * @version	1.0
 * @author	Bruce A. Kern
 */
public class XYZNumber
{
    public final static int size = 3 * ICCProfile.int_size;

	/** x value */public int dwX;	// X tristimulus value
	/** y value */public int dwY;	// Y tristimulus value
	/** z value */public int dwZ;	// Z tristimulus value

    /** Construct from constituent parts. */
    public XYZNumber (int x, int y, int z) {
        dwX = x; dwY = y; dwZ = z; }

    /** Normalization utility */
    public static int DoubleToXYZ ( double x ) {
        return (int) Math.floor(x * 65536.0 + 0.5); }

    /** Normalization utility */
    public static double XYZToDouble (int x) { 
        return (double)x / 65536.0; }

    /** Write to a file */
    public void write (RandomAccessFile raf) throws IOException {
        raf.writeInt (dwX);
        raf.writeInt (dwY);
        raf.writeInt (dwZ); }

    /** String representation of class instance. */
    public String toString () {
        return "[" + dwX + ", " + dwY + ", " + dwZ + "]"; }

    
    /* end class XYZNumber */ }






