/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package icc.types;

import java.io.IOException;
import java.io.RandomAccessFile;
import icc .ICCProfile;

/**
 * This class describes the ICCProfile Version as contained in
 * the header of the ICC Profile.
 * 
 * @see		jj2000.j2k.icc.ICCProfile
 * @see		jj2000.j2k.icc.types.ICCProfileHeader
 * @version	1.0
 * @author	Bruce A. Kern
 */
public class ICCProfileVersion {
    /** Field size */ public final static int size = 4 * ICCProfile.byte_size;
     
	/** Major revision number in binary coded decimal */   public byte uMajor;
    /** Minor revision in high nibble, bug fix revision           
        in low nibble, both in binary coded decimal   */   public byte uMinor;
	
    private byte reserved1;
    private byte reserved2;

    /** Construct from constituent parts. */
    public ICCProfileVersion (byte major, byte minor, byte res1, byte res2) {
        uMajor = major; uMinor = minor; reserved1 = res1; reserved2 = res2; }

    /** Construct from file content. */
    public void write (RandomAccessFile raf) throws IOException {
        raf.write(uMajor); raf.write(uMinor); raf.write(reserved1); raf.write(reserved2);  }

    /** String representation of class instance. */
    public String toString () {
        return "Version " + uMajor + "." + uMinor; }

    /* end class ICCProfileVersion */ }



