/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package icc;

import java.io.InputStream;
import java.io.File;
import java.io.IOException;
import colorspace .ColorSpace;
import colorspace .ColorSpaceException;
import jj2000.j2k.io.RandomAccessIO;

/**
 * This class enables an application to construct an 3 component ICCProfile
 * 
 * @version	1.0
 * @author	Bruce A. Kern
 */

public class ICCMatrixBasedInputProfile extends ICCProfile {
     
    /**
     * Factory method to create ICCMatrixBasedInputProfile based on a
     * suppled profile file.
     *   @param f contains a disk based ICCProfile.
     * @return the ICCMatrixBasedInputProfile
     * @exception ICCProfileInvalidException
     * @exception ColorSpaceException
     */
    public static ICCMatrixBasedInputProfile createInstance (ColorSpace csm) 
        throws ColorSpaceException, ICCProfileInvalidException {
        return new ICCMatrixBasedInputProfile (csm); }

    /**
     * Construct an ICCMatrixBasedInputProfile based on a
     * suppled profile file.
     *   @param f contains a disk based ICCProfile.
     * @exception ColorSpaceException
     * @exception ICCProfileInvalidException
     */
    protected ICCMatrixBasedInputProfile (ColorSpace csm)
        throws ColorSpaceException, ICCProfileInvalidException {
            super (csm); }
    
    /* end class ICCMatrixBasedInputProfile */ }
















