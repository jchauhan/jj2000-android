/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package icc;

import icc .tags.ICCCurveType;
import icc .tags.ICCXYZType;
import icc .tags.ICCTagTable;
import icc .tags.ICCTag;

/**
 * This profile is constructed by parsing an ICCProfile and
 * is the profile actually applied to the image.
 * 
 * @see		jj2000.j2k.icc.ICCProfile
 * @version	1.0
 * @author	Bruce A. Kern
 */
public abstract class RestrictedICCProfile {

    protected static final String eol = System.getProperty("line.separator");

    /**
     * Factory method for creating a RestrictedICCProfile from 
     * 3 component curve and colorant data.
     *   @param rcurve red curve
     *   @param gcurve green curve
     *   @param bcurve blue curve
     *   @param rcolorant red colorant
     *   @param gcolorant green colorant
     *   @param bcolorant blue colorant
     * @return MatrixBasedRestrictedProfile
     */
    public static RestrictedICCProfile createInstance 
        (ICCCurveType rcurve, ICCCurveType gcurve, ICCCurveType bcurve, 
         ICCXYZType rcolorant, ICCXYZType gcolorant, ICCXYZType bcolorant) {

        return   
            MatrixBasedRestrictedProfile.createInstance
            (rcurve, gcurve, bcurve, 
             rcolorant, gcolorant, bcolorant); }

    /**
     * Factory method for creating a RestrictedICCProfile from 
     * gray curve data.
     *   @param gcurve gray curve
     * @return MonochromeInputRestrictedProfile
     */
    public static RestrictedICCProfile createInstance (ICCCurveType gcurve) {
        return  
            MonochromeInputRestrictedProfile.createInstance
            (gcurve); }

    /** Component index       */ protected final static int GRAY  = ICCProfile.GRAY ;
    /** Component index       */ protected final static int RED   = ICCProfile.RED;
    /** Component index       */ protected final static int GREEN = ICCProfile.GREEN;
    /** Component index       */ protected final static int BLUE  = ICCProfile.BLUE ;
    /** input type enumerator */ public    final static int kMonochromeInput =  0;
    /** input type enumerator */ public    final static int kThreeCompInput  =  1;

    /** Curve data    */ public ICCCurveType [] trc; 
    /** Colorant data */ public ICCXYZType	  [] colorant;

    /** Returns the appropriate input type enum. */
    public abstract int getType();

    /**
     * Construct the common state of all gray RestrictedICCProfiles
     *   @param gcurve curve data
     */
    protected RestrictedICCProfile (ICCCurveType gcurve) {
        trc = new ICCCurveType [1];
        colorant = null;
        trc[GRAY] = gcurve;
    }

    /**
     * Construct the common state of all 3 component RestrictedICCProfiles
     * 
     *   @param rcurve red curve
     *   @param gcurve green curve
     *   @param bcurve blue curve
     *   @param rcolorant red colorant
     *   @param gcolorant green colorant
     *   @param bcolorant blue colorant
     */
    protected RestrictedICCProfile (ICCCurveType rcurve, ICCCurveType gcurve, ICCCurveType bcurve, 
                                    ICCXYZType rcolorant, ICCXYZType gcolorant, ICCXYZType bcolorant) {
        trc         = new ICCCurveType [3];
        colorant    = new ICCXYZType [3];

        trc [RED]   = rcurve;
        trc [GREEN] = gcurve;
        trc [BLUE]  = bcurve;

        colorant [RED] = rcolorant;
        colorant [GREEN] = gcolorant;
        colorant [BLUE] = bcolorant;
    }

    /* end class RestrictedICCProfile */ }



