/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package icc.lut;

import icc .tags.ICCCurveType;

/**
 * A Linear 16 bit SRGB to SRGB lut
 * 
 * @version	1.0
 * @author	Bruce A. Kern
 */
public class LookUpTable16LinearSRGBtoSRGB extends LookUpTable16 {     
    
    /**
     * Factory method for creating the lut.
     *   @param wShadowCutoff size of shadow region
     *   @param dfShadowSlope shadow region parameter
     *   @param ksRGBLinearMaxValue size of lut
     *   @param ksRGB8ScaleAfterExp post shadow region parameter
     *   @param ksRGBExponent post shadow region parameter
     *   @param ksRGB8ReduceAfterEx post shadow region parameter
     * @return the lut
     */
    public static LookUpTable16LinearSRGBtoSRGB createInstance (
                                                               int wShadowCutoff, 
                                                               double dfShadowSlope,
                                                               int ksRGBLinearMaxValue, 
                                                               double ksRGB8ScaleAfterExp, 
                                                               double ksRGBExponent, 
                                                               double ksRGB8ReduceAfterEx) { 
        return new LookUpTable16LinearSRGBtoSRGB
            ( wShadowCutoff, 
              dfShadowSlope,
              ksRGBLinearMaxValue, 
              ksRGB8ScaleAfterExp, 
              ksRGBExponent, 
              ksRGB8ReduceAfterEx); 
    }
    
    /**
     * Construct the lut
     *   @param wShadowCutoff size of shadow region
     *   @param dfShadowSlope shadow region parameter
     *   @param ksRGBLinearMaxValue size of lut
     *   @param ksRGB8ScaleAfterExp post shadow region parameter
     *   @param ksRGBExponent post shadow region parameter
     *   @param ksRGB8ReduceAfterExp post shadow region parameter
     */
    protected LookUpTable16LinearSRGBtoSRGB 
        (
         int wShadowCutoff, 
         double dfShadowSlope,
         int ksRGBLinearMaxValue, 
         double ksRGB8ScaleAfterExp, 
         double ksRGBExponent, 
         double ksRGB8ReduceAfterExp) {

        super (ksRGBLinearMaxValue+1, (short)0);

        int i=-1;
        double dfNormalize = 1.0 / (float)ksRGBLinearMaxValue;

        // Generate the final linear-sRGB to non-linear sRGB LUT
        for (i = 0; i <= wShadowCutoff; i++)
            lut[i] = (byte)Math.floor(dfShadowSlope * (double)i + 0.5);

        // Now calculate the rest
        for (; i <= ksRGBLinearMaxValue; i++)
            lut[i] = (byte)Math.floor(ksRGB8ScaleAfterExp  * 
                                      Math.pow((double)i * dfNormalize, ksRGBExponent) 
                                      - ksRGB8ReduceAfterExp + 0.5); }

    /* end class LookUpTable16LinearSRGBtoSRGB */ }


