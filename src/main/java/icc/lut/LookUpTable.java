/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package icc.lut;

import icc .tags.ICCCurveType;


/**
 * Toplevel class for a lut.  All lookup tables must
 * extend this class.
 * 
 * @version	1.0
 * @author	Bruce A. Kern
 */
public abstract class LookUpTable {

    /** End of line string.             */ protected static final String eol = System.getProperty ("line.separator");
    /** The curve data                  */ protected ICCCurveType curve = null;
    /** Number of values in created lut */ protected int          dwNumInput  = 0;
                

    /**
     * For subclass usage.
     *   @param curve The curve data  
     *   @param dwNumInput Number of values in created lut
     */
    protected LookUpTable (
                 ICCCurveType curve,
                 int dwNumInput
                 ) {
        this.curve       = curve;
        this.dwNumInput  = dwNumInput; }

    /* end class LookUpTable */ }
















