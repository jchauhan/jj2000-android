/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package icc.lut;

import icc .tags.ICCCurveType;

/**
 * Toplevel class for a float [] lut.
 * 
 * @version	1.0
 * @author	Bruce A. Kern
 */
public abstract class LookUpTableFP extends LookUpTable {     
    
    /** The lut values. */ public final float [] lut;

    /**
     * Factory method for getting a lut from a given curve.
     *   @param curve  the data
     *   @param dwNumInput the size of the lut 
     * @return the lookup table
     */

    public static LookUpTableFP createInstance (
                 ICCCurveType curve,   // Pointer to the curve data            
                 int dwNumInput        // Number of input values in created LUT
                ) {

        if (curve.nEntries == 1) return new LookUpTableFPGamma  (curve, dwNumInput);
        else                     return new LookUpTableFPInterp (curve, dwNumInput); }

    /**
      * Construct an empty lut
      *   @param dwNumInput the size of the lut t lut.
      *   @param dwMaxOutput max output value of the lut
      */
    protected LookUpTableFP (
                 ICCCurveType curve,   // Pointer to the curve data            
                 int dwNumInput       // Number of input values in created LUT
                 ) { 
        super (curve, dwNumInput);
        lut = new float [dwNumInput]; }
    
    /**
     * lut accessor
     *   @param index of the element
     * @return the lut [index]
     */
    public final float elementAt  ( int index ) {
        return lut [index]; }
    
    /* end class LookUpTableFP */ }
















