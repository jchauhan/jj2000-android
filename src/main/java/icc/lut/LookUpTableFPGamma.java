/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package icc.lut;

import icc .tags.ICCCurveType;

/**
 * Class Description
 * 
 * @version	1.0
 * @author	Bruce A. Kern
 */

public class LookUpTableFPGamma extends LookUpTableFP {     

    double dfE = -1;
    private static final String eol = System.getProperty("line.separator");

    public LookUpTableFPGamma (
                 ICCCurveType curve,   // Pointer to the curve data            
                 int dwNumInput       // Number of input values in created LUT
                 ) {
        super (curve, dwNumInput); 

        // Gamma exponent for inverse transformation
        dfE = ICCCurveType.CurveGammaToDouble(curve.entry(0));
        for (int i = 0; i < dwNumInput; i++)
            lut[i] = (float) Math.pow((double)i / (dwNumInput - 1), dfE); }

    /**
     * Create an abbreviated string representation of a 16 bit lut.
     * @return the lut as a String
     */
    public String toString () {
        StringBuffer rep = new StringBuffer ("[LookUpTableGamma ");
        int row,col;
        rep .append("dfe= " + dfE);
        rep .append(", nentries= " + lut.length);
        return rep.append("]").toString(); }


    /* end class LookUpTableFPGamma */ }
















