/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package icc.lut;

import icc .tags.ICCCurveType;

/**
 * Toplevel class for a byte [] lut.
 * 
 * @version	1.0
 * @author	Bruce A. Kern
 */
public abstract class LookUpTable8 extends LookUpTable {     
    
    /** Maximum output value of the LUT */  protected final byte dwMaxOutput;   // Maximum output value of the LUT
    /** The lut values.                 */  protected final byte [] lut;


    /**
     * Create an abbreviated string representation of a 16 bit lut.
     * @return the lut as a String
     */
    public String toString () {
        StringBuffer rep = new StringBuffer ("[LookUpTable8 ");
        int row,col;
        rep .append("max= " + dwMaxOutput);
        rep .append(", nentries= " + dwMaxOutput);
        return rep.append("]").toString(); }



    public String toStringWholeLut () {
        StringBuffer rep = new StringBuffer ("LookUpTable8" + eol);
        rep .append("maxOutput = " + dwMaxOutput + eol);
        for (int i=0; i<dwNumInput; ++i) 
            rep .append("lut["+i+"] = " + lut[i] + eol);
        return rep.append("]").toString(); }

    protected  LookUpTable8  
        ( int dwNumInput,       // Number of i   nput values in created LUT
          byte dwMaxOutput       // Maximum output value of the LUT   
          ) {
        super (null, dwNumInput);
        lut = new byte [dwNumInput];
        this.dwMaxOutput = dwMaxOutput; }
    

    /**
     * Create the string representation of a 16 bit lut.
     * @return the lut as a String
     */
    protected LookUpTable8 
        ( ICCCurveType curve,   // Pointer to the curve data            
          int dwNumInput,       // Number of input values in created LUT
          byte dwMaxOutput       // Maximum output value of the LUT
          ) { 
        super (curve, dwNumInput);
        this.dwMaxOutput = dwMaxOutput;
        lut = new byte [dwNumInput]; }
    
    /**
     * lut accessor
     *   @param index of the element
     * @return the lut [index]
     */
    public final byte elementAt  ( int index ) {
        return lut [index]; }
    
    /* end class LookUpTable8 */ }
















