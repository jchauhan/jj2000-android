/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package jj2000.j2k.util;

/**
 * This class contains a colleaction of utility static methods for arrays.
 * */
public class ArrayUtil {

    /** The maximum array size to do element by element copying, larger
     * arrays are copyied in a n optimized way. */
    public static final int MAX_EL_COPYING = 8;

    /** The number of elements to copy initially in an optimized array copy */
    public static final int INIT_EL_COPYING = 4;

    /**
     * Reinitializes an int array to the given value in an optimized way. If
     * the length of the array is less than MAX_EL_COPYING, then the array
     * is set element by element in the normal way, otherwise the first
     * INIT_EL_COPYING elements are set element by element and then
     * System.arraycopy is used to set the other parts of the array.
     *
     * @param arr The array to set.
     *
     * @param val The value to set the array to.
     *
     *
     * */
    public static void intArraySet(int arr[], int val) {
        int i,len,len2;
        
        len = arr.length;
        // Set array to 'val' in an optimized way
        if (len < MAX_EL_COPYING) { 
            // Not worth doing optimized way
            for (i=len-1; i>=0; i--) { // Set elements
                arr[i] = val;
            }
        }
        else { // Do in optimized way
            len2 = len>>1;
            for (i=0; i<INIT_EL_COPYING; i++) { // Set first elements
                arr[i] = val;
            }
            for (; i <= len2 ; i<<=1) {
                // Copy values doubling size each time
                System.arraycopy(arr,0,arr,i,i);
            }
            if (i < len) { // Copy values to end
                System.arraycopy(arr,0,arr,i,len-i);
            }
        }
    }

    /**
     * Reinitializes a byte array to the given value in an optimized way. If
     * the length of the array is less than MAX_EL_COPYING, then the array
     * is set element by element in the normal way, otherwise the first
     * INIT_EL_COPYING elements are set element by element and then
     * System.arraycopy is used to set the other parts of the array.
     *
     * @param arr The array to set.
     *
     * @param val The value to set the array to.
     *
     *
     * */
    public static void byteArraySet(byte arr[], byte val) {
        int i,len,len2;
        
        len = arr.length;
        // Set array to 'val' in an optimized way
        if (len < MAX_EL_COPYING) { 
            // Not worth doing optimized way
            for (i=len-1; i>=0; i--) { // Set elements
                arr[i] = val;
            }
        }
        else { // Do in optimized way
            len2 = len>>1;
            for (i=0; i<INIT_EL_COPYING; i++) { // Set first elements
                arr[i] = val;
            }
            for (; i <= len2 ; i<<=1) {
                // Copy values doubling size each time
                System.arraycopy(arr,0,arr,i,i);
            }
            if (i < len) { // Copy values to end
                System.arraycopy(arr,0,arr,i,len-i);
            }
        }
    }

}
