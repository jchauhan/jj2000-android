/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package jj2000.j2k;

/**
 * This exception is thrown whenever a next???? method is called and
 * there is no next element to return.
 *
 */
public class NoNextElementException extends RuntimeException {

    /**
     * Constructs a new <tt>NoNextElementException</tt> exception with no
     * detail message.
     *
     *
     */
    public NoNextElementException() {
        super();
    }

    /**
     * Constructs a new <tt>NoNextElementException</tt> exception with
     * the specified detail message.
     *
     * @param s The detail message.
     *
     * */
    public NoNextElementException(String s) {
        super(s);
    }

}


