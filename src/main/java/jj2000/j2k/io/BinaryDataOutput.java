/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.io;

import java.io.*;

/**
 * This interface defines the output of binary data to streams and/or files.
 *
 * <P>Byte level output (i.e., for byte, int, long, float, etc.) should
 * always be byte aligned. For example, a request to write an
 * <tt>int</tt> should always realign the output at the byte level.
 *
 * <P>The implementation of this interface should clearly define if
 * multi-byte output data is written in little- or big-endian byte
 * ordering (least significant byte first or most significant byte
 * first, respectively).
 *
 * @see EndianType
 * */
public interface BinaryDataOutput {

    /**
     * Should write the byte value of <tt>v</tt> (i.e., 8 least
     * significant bits) to the output. Prior to writing, the output
     * should be realigned at the byte level.
     *
     * <P>Signed or unsigned data can be written. To write a signed
     * value just pass the <tt>byte</tt> value as an argument. To
     * write unsigned data pass the <tt>int</tt> value as an argument
     * (it will be automatically casted, and only the 8 least
     * significant bits will be written).
     *
     * @param v The value to write to the output
     *
     * @exception IOException If an I/O error ocurred.
     *     
     *
     *
     */
    public void writeByte(int v) throws IOException;

    /**
     * Should write the short value of <tt>v</tt> (i.e., 16 least
     * significant bits) to the output. Prior to writing, the output
     * should be realigned at the byte level.
     *
     * <P>Signed or unsigned data can be written. To write a signed
     * value just pass the <tt>short</tt> value as an argument. To
     * write unsigned data pass the <tt>int</tt> value as an argument
     * (it will be automatically casted, and only the 16 least
     * significant bits will be written).
     *
     * @param v The value to write to the output
     *
     * @exception IOException If an I/O error ocurred.
     *     
     *
     *
     */
    public void writeShort(int v) throws IOException;

    /**
     * Should write the int value of <tt>v</tt> (i.e., the 32 bits) to
     * the output. Prior to writing, the output should be realigned at
     * the byte level.
     *
     * @param v The value to write to the output
     *
     * @exception IOException If an I/O error ocurred.
     *     
     *
     *
     */
    public void writeInt(int v) throws IOException;

    /**
     * Should write the long value of <tt>v</tt> (i.e., the 64 bits)
     * to the output. Prior to writing, the output should be realigned
     * at the byte level.
     *
     * @param v The value to write to the output
     *
     * @exception IOException If an I/O error ocurred.
     *     
     *
     *
     */
    public void writeLong(long v) throws IOException;

    /**
     * Should write the IEEE float value <tt>v</tt> (i.e., 32 bits) to
     * the output. Prior to writing, the output should be realigned at
     * the byte level.
     *
     * @param v The value to write to the output
     *
     * @exception IOException If an I/O error ocurred.
     *     
     *
     *
     */
    public void writeFloat(float v) throws IOException;

    /**
     * Should write the IEEE double value <tt>v</tt> (i.e., 64 bits)
     * to the output. Prior to writing, the output should be realigned
     * at the byte level.
     *
     * @param v The value to write to the output
     *
     * @exception IOException If an I/O error ocurred.
     *     
     *
     *
     */
    public void writeDouble(double v) throws IOException;
    
    /**
     * Returns the endianness (i.e., byte ordering) of the implementing
     * class. Note that an implementing class may implement only one
     * type of endianness or both, which would be decided at creatiuon
     * time.
     *
     * @return Either <tt>EndianType.BIG_ENDIAN</tt> or
     * <tt>EndianType.LITTLE_ENDIAN</tt>
     *
     * @see EndianType
     *
     *
     *
     */
    public int getByteOrdering();

    /**
     * Any data that has been buffered must be written, and the stream should
     * be realigned at the byte level.
     *
     * @exception IOException If an I/O error ocurred.
     *     
     *
     * */
    public void flush() throws IOException;
}
