/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.io;

/**
 * This interface defines constants for the two types of byte
 * ordering: little- and big-endian.
 *
 * <P>Little-endian is least significant byte first.
 *
 * <P>Big-endian is most significant byte first.
 *
 * <P>This interface defines the constants only. In order to use the
 * constants in any other class you can either use the fully qualified 
 * name (e.g., <tt>EndianType.LITTLE_ENDIAN</tt>) or declare this
 * interface in the implements clause of the class and then access the 
 * identifier directly.
 *
 */
public interface EndianType {

    /** Identifier for big-endian byte ordering (i.e. most significant 
     * byte first) */
    public static final int BIG_ENDIAN = 0;

    /** Identifier for little-endian byte ordering (i.e. least
     * significant byte first) */
    public static final int LITTLE_ENDIAN = 1;
}
