/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.io;

import java.io.*;

/**
 * This abstract class defines the interface to perform random access I/O. It
 * implements the <tt>BinaryDataInput</tt> and <tt>BinaryDataOutput</tt>
 * interfaces so that binary data input/output can be performed.
 *
 * <p>This interface supports streams of up to 2 GB in length.</p>
 *
 * @see BinaryDataInput
 * @see BinaryDataOutput
 * */
public interface RandomAccessIO
    extends BinaryDataInput, BinaryDataOutput {

    /**
     * Closes the I/O stream. Prior to closing the stream, any buffered data
     * (at the bit and byte level) should be written.
     *
     * @exception IOException If an I/O error ocurred. 
     * */
    public void close() throws IOException;

    /**
     * Returns the current position in the stream, which is the position from
     * where the next byte of data would be read. The first byte in the stream
     * is in position <tt>0</tt>.
     *
     * @return The offset of the current position, in bytes.
     *
     * @exception IOException If an I/O error ocurred.
     * */
    public int getPos() throws IOException;

    /**
     * Returns the current length of the stream, in bytes, taking into account
     * any buffering.
     *
     * @return The length of the stream, in bytes.
     *
     * @exception IOException If an I/O error ocurred. 
     * */
    public int length() throws IOException;

    /**
     * Moves the current position for the next read or write operation to
     * offset. The offset is measured from the beginning of the stream. The
     * offset may be set beyond the end of the file, if in write mode. Setting
     * the offset beyond the end of the file does not change the file
     * length. The file length will change only by writing after the offset
     * has been set beyond the end of the file.
     *
     * @param off The offset where to move to.
     *
     * @exception EOFException If in read-only and seeking beyond EOF.
     *
     * @exception IOException If an I/O error ocurred.
     * */
    public void seek(int off) throws IOException;

    /**
     * Reads a byte of data from the stream. Prior to reading, the stream is
     * realigned at the byte level.
     *
     * @return The byte read, as an int.
     *
     * @exception EOFException If the end-of file was reached.
     *
     * @exception IOException If an I/O error ocurred.
     * */
    public int read() throws EOFException, IOException;

    /**
     * Reads up to len bytes of data from this file into an array of
     * bytes. This method reads repeatedly from the stream until all the bytes
     * are read. This method blocks until all the bytes are read, the end of
     * the stream is detected, or an exception is thrown.
     *
     * @param b The buffer into which the data is to be read. It must be long
     * enough.
     *
     * @param off The index in 'b' where to place the first byte read.
     *
     * @param len The number of bytes to read.
     *
     * @exception EOFException If the end-of file was reached before
     * getting all the necessary data.
     *
     * @exception IOException If an I/O error ocurred.
     * */
    public void readFully(byte b[], int off, int len) throws IOException;

    /**
     * Writes a byte to the stream. Prior to writing, the stream is realigned
     * at the byte level.
     *
     * @param b The byte to write. The lower 8 bits of <tt>b</tt> are
     * written.
     *
     * @exception IOException If an I/O error ocurred. 
     * */
    public void write(int b) throws IOException;
}
