/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.entropy.decoder;

import jj2000.j2k.quantization.dequantizer.*;
import jj2000.j2k.codestream.reader.*;
import jj2000.j2k.wavelet.synthesis.*;
import jj2000.j2k.codestream.*;
import jj2000.j2k.entropy.*;
import jj2000.j2k.image.*;
import jj2000.j2k.io.*;
import jj2000.j2k.*;
import java.io.*;

/**
 * This is the abstract class from which all entropy decoders must
 * inherit. This class implements the 'MultiResImgData', therefore it has the
 * concept of a current tile and all operations are performed on the current
 * tile.
 *
 * <p>Default implementations of the methods in 'MultiResImgData' are provided
 * through the 'MultiResImgDataAdapter' abstract class.</p>
 *
 * <p>Sign magnitude representation is used (instead of two's complement) for
 * the output data. The most significant bit is used for the sign (0 if
 * positive, 1 if negative). Then the magnitude of the quantized coefficient
 * is stored in the next most significat bits. The most significant magnitude
 * bit corresponds to the most significant bit-plane and so on.</p
 *
 * @see MultiResImgData
 * @see MultiResImgDataAdapter
 * */
public abstract class EntropyDecoder extends MultiResImgDataAdapter
    implements CBlkQuantDataSrcDec {

    /** The prefix for entropy decoder optiojns: 'C' */
    public final static char OPT_PREFIX = 'C';

    /** The list of parameters that is accepted by the entropy
     * decoders. They start with 'C'. */
    private final static String [][] pinfo = {
        {"Cverber", "[on|off]",
         "Specifies if the entropy decoder should be verbose about detected "+
         "errors. If 'on' a message is printed whenever an error is detected.",
         "on"},
        {"Cer", "[on|off]",
         "Specifies if error detection should be performed by the entropy "+
         "decoder engine. If errors are detected they will be concealed and "+
         "the resulting distortion will be less important. Note that errors "+
         "can only be detected if the encoder that generated the data "+
         "included error resilience information.", "on"},
    };

    /** The bit stream transport from where to get the compressed data
     * (the source) */
    protected CodedCBlkDataSrcDec src;

    /**
     * Initializes the source of compressed data.
     *
     * @param src From where to obtain the compressed data.
     * */
    public EntropyDecoder(CodedCBlkDataSrcDec src) {
        super(src);
        this.src = src;
    }

    /**
     * Returns the subband tree, for the specified tile-component. This method
     * returns the root element of the subband tree structure, see Subband and
     * SubbandSyn. The tree comprises all the available resolution levels.
     *
     * <P>The number of magnitude bits ('magBits' member variable) for
     * each subband is not initialized.
     *
     * @param t The index of the tile, from 0 to T-1.
     *
     * @param c The index of the component, from 0 to C-1.
     *
     * @return The root of the tree structure.
     * */
    public SubbandSyn getSynSubbandTree(int t,int c) {
        return src.getSynSubbandTree(t,c);
    }

    /**
     * Returns the horizontal code-block partition origin. Allowable values
     * are 0 and 1, nothing else.
     * */
    public int getCbULX() {
        return src.getCbULX();
    }

    /**
     * Returns the vertical code-block partition origin. Allowable values are
     * 0 and 1, nothing else.
     * */
    public int getCbULY() {
        return src.getCbULY();
    }

    /**
     * Returns the parameters that are used in this class and
     * implementing classes. It returns a 2D String array. Each of the
     * 1D arrays is for a different option, and they have 3
     * elements. The first element is the option name, the second one
     * is the synopsis and the third one is a long description of what
     * the parameter is. The synopsis or description may be 'null', in
     * which case it is assumed that there is no synopsis or
     * description of the option, respectively. Null may be returned
     * if no options are supported.
     *
     * @return the options name, their synopsis and their explanation, 
     * or null if no options are supported.
     * */
    public static String[][] getParameterInfo() {
        return pinfo;
    }

}
