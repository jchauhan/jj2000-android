/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.entropy.encoder;

/**
 * This class holds information about each layer that is to be, or has already
 * been, allocated . It is used in the rate-allocation process to keep the
 * necessary layer information. It is used by EBCOTRateAllocator.
 *
 * @see EBCOTRateAllocator
 * */
class EBCOTLayer {
    /**
     * This is the maximum number of bytes that should be allocated for this
     * and previous layers. This is actually the target length for the layer.
     * */
    int maxBytes;
    
    /**
     * The actual number of bytes which are consumed by the the current and
     * any previous layers. This is the result from a simulation when the
     * threshold for the layer has been set.
     * */
    int actualBytes;
    
    /**
     * If true the `maxBytes' value is the hard maximum and the threshold is
     * determined iteratively. If false the `maxBytes' value is a target
     * bitrate and the threshold is estimated from summary information
     * accumulated during block coding.
     * */
    boolean optimize;
    
    /** 
     * The rate-distortion threshold associated with the bit-stream
     * layer. When set the layer includes data up to the truncation points
     * that have a slope no smaller than 'rdThreshold'.
     * */        
    float rdThreshold;
}

