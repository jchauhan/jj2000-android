/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k;

/**
 * This exception is thrown whenever a feature or functionality that
 * has not been implemented is calle.
 *
 * <P>Its purpose it is to ease the development and testing process. A
 * class that partially implements its functionality should throw a
 * <tt>NotImplementedError</tt> when a method that has not yet
 * been implemented is called.
 *
 * <P>This class is made a subclass of <tt>Error</tt> since it should
 * never be caught by an application. There is no need to declare this
 * exception in the <tt>throws</tt> clause of a method.
 *
 * @see Error
 */
public class NotImplementedError extends Error {

    /**
     * Constructs a new <tt>NotImplementedError</tt> exception with
     * the default detail message. The message is:
     *
     * <P><I>The called method has not been implemented yet. Sorry!</I>
     *
     *
     */
    public NotImplementedError() {
        super("The called method has not been implemented yet. Sorry!");
    }

    /**
     * Constructs a new <tt>NotImplementedError</tt> exception with
     * the specified detail message <tt>m</tt>.
     *
     * @param m The detail message to use
     *
     *
     */
    public NotImplementedError(String m) {
        super(m);
    }
}
