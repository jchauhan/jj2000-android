/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.codestream;

import jj2000.j2k.image.Coord;

/**
 * This class is used to store the coordinates of objects such as code-blocks
 * or precincts. As this is an abstract class, it cannot be used directly but
 * derived classes have been created for code-blocks and packets
 * (CBlkCoordInfo and PrecCoordInfo).
 *
 * @see PrecCoordInfo
 * @see CBlkCoordInfo
 * */
public abstract class CoordInfo {

    /** Horizontal upper left coordinate in the subband */
    public int ulx;

    /** Vertical upper left coordinate in the subband */ 
    public int uly;

    /** Object's width */
    public int w;

    /** Object's height */
    public int h;
    
    /** 
     * Constructor. Creates a CoordInfo object.
     *
     * @param ulx The horizontal upper left coordinate in the subband
     *
     * @param uly The vertical upper left coordinate in the subband
     *
     * @param w The width
     *
     * @param h The height
     *
     * @param idx The object's index
     * */
    public CoordInfo(int ulx, int uly, int w, int h) {
        this.ulx = ulx;
        this.uly = uly;
        this.w = w;
        this.h = h;
    }
    
    /** Empty contructor */
    public CoordInfo() { }

    /** 
     * Returns object's information in a String 
     * 
     * @return String with object's information
     * */
    public String toString() {
        return "ulx="+ulx+",uly="+uly+",w="+w+",h="+h;
    }
}
