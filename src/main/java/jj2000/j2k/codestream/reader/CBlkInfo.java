/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.codestream.reader;

import java.util.*;

/**
 * This class contains location of code-blocks' piece of codewords (there is
 * one piece per layer) and some other information.
 * */
public class CBlkInfo {
    
    /** Upper-left x-coordinate of the code-block (relative to the tile) */
    public int ulx;

    /** Upper-left y-coordinate of the code-block (relative to the tile) */
    public int uly;

    /** Width of the code-block */
    public int w;

    /** Height of the code-block */
    public int h;

    /** The number of most significant bits which are skipped for this
     * code-block (= Mb-1-bitDepth). */
    public int msbSkipped;
    
    /** Length of each piece of code-block's codewords */
    public int[] len;

    /** Offset of each piece of code-block's codewords in the file */
    public int[] off;

    /** The number of truncation point for each layer */
    public int[] ntp;

    /** The cumulative number of truncation points */
    public int ctp;

    /** The length of each segment (used with regular termination or in
     * selective arithmetic bypass coding mode) */
    public int[][] segLen;

    /** Index of the packet where each layer has been found */
    public int[] pktIdx;

    /** 
     * Constructs a new instance with specified number of layers and
     * code-block coordinates. The number corresponds to the maximum piece of
     * codeword for one code-block.
     *
     * @param ulx The uper-left x-coordinate
     *
     * @param uly The uper-left y-coordinate
     *
     * @param w Width of the code-block
     *
     * @param h Height of the code-block
     *
     * @param nl The number of layers
     * */
    public CBlkInfo(int ulx,int uly,int w,int h,int nl) {
        this.ulx = ulx;
        this.uly = uly;
        this.w = w;
        this.h = h;
        off = new int[nl];
        len = new int[nl];
        ntp = new int[nl];
        segLen = new int[nl][];
	pktIdx = new int[nl];
	for(int i=nl-1;i>=0;i--) {
	    pktIdx[i] = -1;
        }
    }

    /** 
     * Adds the number of new truncation for specified layer.
     *
     * @param l layer index
     *
     * @param newtp Number of new truncation points 
     * */
    public void addNTP(int l,int newtp){
        ntp[l] = newtp;
        ctp = 0;
        for(int lIdx=0; lIdx<=l; lIdx++){
            ctp += ntp[lIdx];
        }
    }

    /**
     * Object information in a string.
     *
     * @return Object information
     * */
    public String toString(){
        String string = "(ulx,uly,w,h)= ("+ulx+","+uly+","+w+","+h;
        string += ") "+msbSkipped+" MSB bit(s) skipped\n";
        if( len!=null )
            for(int i=0; i<len.length; i++){
                string += "\tl:"+i+", start:"+off[i]+
                    ", len:"+len[i]+", ntp:"+ntp[i]+", pktIdx="+
		    pktIdx[i];
                if(segLen!=null && segLen[i]!=null){
                    string += " { ";
                    for(int j=0; j<segLen[i].length; j++)
                        string += segLen[i][j]+" ";
                    string += "}";
                }
                string += "\n";
            }
        string += "\tctp="+ctp;
        return string;
    }
}
