/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.codestream;

/** 
 * Class that holds precinct coordinates and references to contained
 * code-blocks in each subband. 
 * */
public class PrecInfo {
    
    /** Precinct horizontal upper-left coordinate in the reference grid */
    public int rgulx;
    
    /** Precinct vertical upper-left coordinate in the reference grid */
    public int rguly;
    
    /** Precinct width reported in the reference grid */
    public int rgw;

    /** Precinct height reported in the reference grid */
    public int rgh;

    /** Precinct horizontal upper-left coordinate in the corresponding
     * resolution level*/
    public int ulx;

    /** Precinct vertical upper-left coordinate in the corresponding
     * resolution level*/
    public int uly;

    /** Precinct width in the corresponding resolution level */
    public int w;
    
    /** Precinct height in the corresponding resolution level */
    public int h;

    /** Resolution level index */
    public int r;

    /** Code-blocks belonging to this precinct in each subbands of the
     * resolution level */
    public CBlkCoordInfo[][][] cblk;

    /** Number of code-blocks in each subband belonging to this precinct */
    public int[] nblk;

    /** 
     * Class constructor.
     *
     * @param r Resolution level index.
     * @param ulx Precinct horizontal offset.
     * @param uly Precinct vertical offset.
     * @param w Precinct width.
     * @param h Precinct height.
     * @param rgulx Precinct horizontal offset in the image reference grid.
     * @param rguly Precinct horizontal offset in the image reference grid.
     * @param rgw Precinct width in the reference grid.
     * @param rgh Precinct height in the reference grid.
     * */
    public PrecInfo(int r,int ulx,int uly,int w,int h,int rgulx,int rguly,
                    int rgw,int rgh) {
        this.r = r;
        this.ulx = ulx;
        this.uly = uly;
        this.w = w;
        this.h = h;
        this.rgulx = rgulx;
        this.rguly = rguly;
        this.rgw = rgw;
        this.rgh = rgh;

        if (r==0) {
            cblk = new CBlkCoordInfo[1][][];
            nblk = new int[1];
        } else {
            cblk = new CBlkCoordInfo[4][][];
            nblk = new int[4];
        }
    }

    /** 
     * Returns PrecInfo object information in a String
     *
     * @return PrecInfo information 
     * */
    public String toString() {
        return "ulx="+ulx+",uly="+uly+",w="+w+",h="+h+",rgulx="+rgulx+
            ",rguly="+rguly+",rgw="+rgw+",rgh="+rgh;
    }
}
