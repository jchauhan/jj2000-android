/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.codestream;

import jj2000.j2k.image.*;

/**
 * This class is used to store the coordinates of code-blocks.
 * */
public class CBlkCoordInfo extends CoordInfo {

    /** The code-block horizontal and vertical indexes */
    public Coord idx;
    
    /** 
     * Constructor. Creates a CBlkCoordInfo object.
     * */
    public CBlkCoordInfo() {
        this.idx = new Coord(); 
    }

    /** 
     * Constructor. Creates a CBlkCoordInfo object width specified code-block
     * vertical and horizontal indexes.
     *
     * @param m Code-block vertical index.
     *
     * @param n Code-block horizontal index.
     * */
    public CBlkCoordInfo(int m,int n) {
        this.idx = new Coord(n,m);
    }
    
    /** 
     * Returns code-block's information in a String 
     * 
     * @return String with code-block's information
     * */
    public String toString() {
        return super.toString() + ",idx="+idx;
    }
}
