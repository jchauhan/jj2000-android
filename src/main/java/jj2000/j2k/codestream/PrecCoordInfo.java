/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.codestream;

/**
 * This class is used to store the coordinates of precincts.
 * */
public class PrecCoordInfo extends CoordInfo {

    /** Horizontal upper left coordinate in the reference grid */
    public int xref;
    
    /** Vertical upper left coordinate on the reference grid */
    public int yref;
    
    /** 
     * Constructor. Creates a PrecCoordInfo object.
     *
     * @param ulx Horizontal upper left coordinate in the subband
     *
     * @param uly Vertical upper left coordinate in the subband
     *
     * @param w Precinct's width
     *
     * @param h Precinct's height
     *
     * @param xref The horizontal coordinate on the reference grid 
     *
     * @param yref The vertical coordinate on the reference grid 
     * */
    public PrecCoordInfo(int ulx,int uly,int w,int h,int xref,int yref) {
        super(ulx,uly,w,h);
        this.xref = xref;
        this.yref = yref;
    }
    
    /** 
     * Empty Constructor. Creates an empty PrecCoordInfo object.
     * */
    public PrecCoordInfo() {
        super();
    }
    
    /** 
     * Returns precinct's information in a String 
     * 
     * @return String with precinct's information
     * */
    public String toString() {
        return super.toString() + ", xref="+xref+", yref="+yref;
    }
}
