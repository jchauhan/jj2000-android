/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package jj2000.j2k.quantization;

/**
 * This interface defines the IDs of the possible quantization types. JPEG
 * 2000 part I support only the scalar quantization with dead zone. However
 * other quantization type may be defined in JPEG 2000 extensions (for
 * instance Trellis Coded Quantization).
 *
 * <P>This interface defines the constants only. In order to use the
 * constants in any other class you can either use the fully qualified
 * name (e.g., <tt>QuantizationType.Q_TYPE_SCALAR_DZ</tt>) or declare
 * this interface in the implements clause of the class and then
 * access the identifier directly.
 * */
public interface QuantizationType {

    /** The ID of the scalar deadzone dequantizer */
    public final static int Q_TYPE_SCALAR_DZ = 0;

}
