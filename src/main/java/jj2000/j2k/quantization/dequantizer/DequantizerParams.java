/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package jj2000.j2k.quantization.dequantizer;

import jj2000.j2k.entropy.decoder.*;
import jj2000.j2k.util.*;

/**
 * This is the generic ineterface for dequantization parameters. Generally,
 * for each type of dequantizer, there should be a corresponding class to
 * store its parameters. The parameters are those that come from the bit
 * stream header, that concern dequantization.
 * */
public abstract class DequantizerParams {

    /**
     * Returns the type of the dequantizer for which the parameters are. The
     * types are defined in the Dequantizer class.
     *
     * @return The type of the dequantizer for which the parameters
     * are.
     *
     * @see Dequantizer
     * */
    public abstract int getDequantizerType();

}

