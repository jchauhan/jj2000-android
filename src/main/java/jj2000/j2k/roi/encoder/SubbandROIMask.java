/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.roi.encoder;

import jj2000.j2k.codestream.writer.*;
import jj2000.j2k.wavelet.analysis.*;
import jj2000.j2k.quantization.*;
import jj2000.j2k.wavelet.*;
import jj2000.j2k.image.*;
import jj2000.j2k.util.*;
import jj2000.j2k.roi.*;

/** 
 * This abstract class describes the ROI mask for a single subband. Each
 * object of the class contains the mask for a particular subband and also has
 * references to the masks of the children subbands of the subband
 * corresponding to this mask.  */
public abstract class SubbandROIMask{

    /** The subband masks of the child LL */
    protected SubbandROIMask ll;

    /** The subband masks of the child LH */
    protected SubbandROIMask lh;

    /** The subband masks of the child HL */
    protected SubbandROIMask hl;

    /** The subband masks of the child HH */
    protected SubbandROIMask hh;

    /** Flag indicating whether this subband mask is a node or not */
    protected boolean isNode;

    /** Horizontal uper-left coordinate of the subband mask */
    public int ulx;

    /** Vertical uper-left coordinate of the subband mask */
    public int uly;

    /** Width of the subband mask */
    public int w;

    /** Height of the subband mask */
    public int h;

    /**
     * The constructor of the SubbandROIMask takes the dimensions of the
     * subband as parameters
     *
     * @param ulx The upper left x coordinate of corresponding subband
     *
     * @param uly The upper left y coordinate of corresponding subband
     *
     * @param w The width of corresponding subband
     *
     * @param h The height of corresponding subband
     * */
    public SubbandROIMask(int ulx, int uly, int w, int h){
        this.ulx=ulx;
        this.uly=uly;
        this.w=w;
        this.h=h;
    }

    /**
     * Returns a reference to the Subband mask element to which the specified
     * point belongs. The specified point must be inside this (i.e. the one
     * defined by this object) subband mask. This method searches through the
     * tree.
     *
     * @param x horizontal coordinate of the specified point.
     *
     * @param y horizontal coordinate of the specified point.
     * */
    public SubbandROIMask getSubbandRectROIMask(int x, int y) {
        SubbandROIMask cur,hhs;

        // Check that we are inside this subband
        if (x < ulx || y < uly || x >= ulx+w || y >= uly+h) {
            throw new IllegalArgumentException();
        }

        cur = this;
        while (cur.isNode) {
            hhs = cur.hh;
            // While we are still at a node -> continue
            if (x < hhs.ulx) {
                // Is the result of horizontal low-pass
                if (y < hhs.uly) {
                    // Vertical low-pass
                    cur = cur.ll;
                }
                else {
                    // Vertical high-pass
                    cur = cur.lh;
                }
            }
            else {
                // Is the result of horizontal high-pass
                if (y < hhs.uly) {
                    // Vertical low-pass
                    cur = cur.hl;
                }
                else {
                    // Vertical high-pass
                    cur = cur.hh;
                }
            }                
        }
        return cur;
    }
}
        
        
 

    


    


  
