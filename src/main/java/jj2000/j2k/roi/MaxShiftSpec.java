/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.roi;

import java.util.*;
import jj2000.j2k.*;

/**
 * This class contains the maxshift scaling value for each tile-component.
 * The scaling values used are calculated in the ROIScaler class
 * */

public class MaxShiftSpec extends ModuleSpec{

    /**
     * Constructs a 'ModuleSpec' object, initializing all the components and 
     * tiles to the 'SPEC_DEF' spec type, for the specified number of 
     * components and tiles.
     *
     * @param nt The number of tiles
     *
     * @param nc The number of components
     *
     * @param type the type of the specification module i.e. tile specific,
     * component specific or both.
     * */
    public MaxShiftSpec(int nt, int nc, byte type) {
      super(nt, nc, type);
    }
}
