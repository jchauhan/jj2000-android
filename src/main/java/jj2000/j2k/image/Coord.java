/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.image;

/**
 * This class represents 2-D coordinates.
 * */
public class Coord {
    /** The horizontal coordinate */
    public int x;

    /** The vertical coordinate */
    public int y;

    /**
     * Creates a new coordinate object given with the (0,0) coordinates
     * */
    public Coord() { }

    /**
     * Creates a new coordinate object given the two coordinates.
     *
     * @param x The horizontal coordinate.
     *
     * @param y The vertical coordinate.
     * */
    public Coord(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    /**
     * Creates a new coordinate object given another Coord object i.e. copy 
     * constructor
     *
     * @param c The Coord object to be copied.
     * */
    public Coord(Coord c) {
        this.x = c.x;
        this.y = c.y;
    }

    /**
     * Returns a string representation of the object coordinates
     *
     * @return The vertical and the horizontal coordinates
     * */
    public String toString(){
	return "("+x+","+y+")";
    }
}
