/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.image;

import jj2000.j2k.image.invcomptransf.*;
import jj2000.j2k.wavelet.*;
import jj2000.j2k.util.*;
import jj2000.j2k.*;

import java.util.*;

/**
 * This class extends the ModuleSpec class in order to hold tile
 * specifications for multiple component transformation
 *
 * @see ModuleSpec
 * */
public class CompTransfSpec extends ModuleSpec {
    
    /** 
     * Constructs an empty 'CompTransfSpec' with the specified number of tiles
     * and components. This constructor is called by the decoder. Note: The
     * number of component is here for symmetry purpose. It is useless since
     * only tile specifications are meaningful.
     *
     * @param nt Number of tiles
     *
     * @param nc Number of components
     *
     * @param type the type of the specification module i.e. tile specific,
     * component specific or both.
     * */
    public CompTransfSpec(int nt, int nc, byte type){
	super(nt, nc, type);
    }


    /** 
     * Check if component transformation is used in any of the tiles. This
     * method must not be used by the encoder.
     *
     * @return True if a component transformation is used in at least on
     * tile.
     * */
    public boolean isCompTransfUsed(){
        if( ((Integer)def).intValue() != InvCompTransf.NONE ){
            return true;
        }
     
        if(tileDef!=null){
            for(int t=nTiles-1; t>=0; t--){
                if(tileDef[t]!=null && 
                   ( ((Integer)tileDef[t]).intValue() != InvCompTransf.NONE) ){
                    return true;
                }
            }
        }
        return false;
    }
}
