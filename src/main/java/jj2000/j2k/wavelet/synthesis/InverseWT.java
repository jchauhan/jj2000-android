/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.wavelet.synthesis;

import jj2000.j2k.quantization.dequantizer.*;
import jj2000.j2k.wavelet.*;
import jj2000.j2k.decoder.*;
import jj2000.j2k.image.*;
import jj2000.j2k.util.*;
import jj2000.j2k.*;

/**
 * This abstract class extends the WaveletTransform one with the specifics of
 * inverse wavelet transforms.
 *
 * <p>The image can be reconstructed at different resolution levels. This is
 * controlled by the setResLevel() method. All the image, tile and component
 * dimensions are relative the the resolution level being used. The number of
 * resolution levels indicates the number of wavelet recompositions that will
 * be used, if it is equal as the number of decomposition levels then the full
 * resolution image is reconstructed.</p>
 *
 * <p>It is assumed in this class that all tiles and components the same
 * reconstruction resolution level. If that where not the case the
 * implementing class should have additional data structures to store those
 * values for each tile. However, the 'recResLvl' member variable always
 * contain the values applicable to the current tile, since many methods
 * implemented here rely on them.</p>
 * */
public abstract class InverseWT extends InvWTAdapter implements BlkImgDataSrc {

    /**
     * Initializes this object with the given source of wavelet
     * coefficients. It initializes the resolution level for full resolutioin
     * reconstruction (i.e. the maximum resolution available from the 'src'
     * source).
     *
     * <p>It is assumed here that all tiles and components have the same
     * reconstruction resolution level. If that was not the case it should be
     * the value for the current tile of the source.</p>
     *
     * @param src from where the wavelet coefficinets should be obtained.
     *
     * @param decSpec The decoder specifications
     * */
    public InverseWT(MultiResImgData src,DecoderSpecs decSpec){
        super(src,decSpec);
    }
    
    /**
     * Creates an InverseWT object that works on the data type of the source,
     * with the special additional parameters from the parameter
     * list. Currently the parameter list is ignored since no special
     * parameters can be specified for the inverse wavelet transform yet.
     *
     * @param src The source of data for the inverse wavelet
     * transform.
     *
     * @param pl The parameter list containing parameters applicable to the
     * inverse wavelet transform (other parameters can also be present).
     * */
    public static InverseWT createInstance(CBlkWTDataSrcDec src,
                                           DecoderSpecs decSpec) {

        // full page wavelet transform
        return new InvWTFull(src,decSpec);
    }
}
