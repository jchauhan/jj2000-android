/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.wavelet.synthesis;

import jj2000.j2k.wavelet.*;

/**
 * This interface extends the WaveletTransform with the specifics of inverse
 * wavelet transforms. Classes that implement inverse wavelet transfoms should
 * implement this interface.
 *
 * <p>This class does not define the methods to transfer data, just the
 * specifics to inverse wavelet transform. Different data transfer methods are
 * envisageable for different transforms.</p>
 * */
public interface InvWT extends WaveletTransform {

    /**
     * Sets the image reconstruction resolution level. A value of 0 means
     * reconstruction of an image with the lowest resolution (dimension)
     * available.
     *
     * <p>Note: Image resolution level indexes may differ from tile-component
     * resolution index. They are indeed indexed starting from the lowest
     * number of decomposition levels of each component of each tile.</p>
     *
     * <p>Example: For an image (1 tile) with 2 components (component 0 having
     * 2 decomposition levels and component 1 having 3 decomposition levels),
     * the first (tile-) component has 3 resolution levels and the second one
     * has 4 resolution levels, whereas the image has only 3 resolution levels
     * available.</p>
     *
     * @param rl The image resolution level.
     *
     * @return The vertical coordinate of the image origin in the canvas
     * system, on the reference grid.
     * */
    public void setImgResLevel(int rl);
}
