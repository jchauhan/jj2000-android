/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.wavelet.synthesis;

/**
 * This interface extends the MultiResImgData interface with the methods that
 * are necessary for inverse wavelet data (i.e. data that is the source to an
 * inverse wavlet trasnform).
 * */
public interface InvWTData extends MultiResImgData {

    /**
     * Returns the subband tree, for the specified tile-component. This method
     * returns the root element of the subband tree structure, see 'Subband'
     * and 'SubbandSyn'. The tree comprises all the available resolution
     * levels.
     *
     * @param t The index of the tile, from 0 to T-1.
     *
     * @param c The index of the component, from 0 to C-1.
     *
     * @return The root of the tree structure.
     * */
    public SubbandSyn getSynSubbandTree(int t,int c);

    /**
     * Returns the horizontal code-block partition origin. Allowable values
     * are 0 and 1, nothing else.
     * */
    public int getCbULX();

    /**
     * Returns the vertical code-block partition origin Allowable values are 0
     * and 1, nothing else.
     * */
    public int getCbULY();
}
