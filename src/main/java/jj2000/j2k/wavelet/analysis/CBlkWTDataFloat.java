/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.wavelet.analysis;

import jj2000.j2k.image.*;

/**
 * This is an implementation of the 'CBlkWTData' abstract class for 32 bit
 * floating point data (float).
 *
 * <p>The methods in this class are declared final, so that they can be
 * inlined by inlining compilers.</p>
 *
 * @see CBlkWTData
 * */
public class CBlkWTDataFloat extends CBlkWTData {

    /** The array where the data is stored */
    public float[] data;

    /**
     * Returns the identifier of this data type, <tt>TYPE_FLOAT</tt>, as
     * defined in <tt>DataBlk</tt>.
     *
     * @return The type of data stored. Always <tt>DataBlk.TYPE_FLOAT</tt>
     *
     * @see DataBlk#TYPE_FLOAT
     * */
    public final int getDataType() {
        return DataBlk.TYPE_FLOAT;
    }

    /**
     * Returns the array containing the data, or null if there is no data
     * array. The returned array is a float array.
     *
     * @return The array of data (a float[]) or null if there is no data.
     * */
    public final Object getData() {
        return data;
    }

    /**
     * Returns the array containing the data, or null if there is no data
     * array.
     *
     * @return The array of data or null if there is no data.
     * */
    public final float[] getDataFloat() {
        return data;
    }

    /**
     * Sets the data array to the specified one. The provided array must be a
     * float array, otherwise a ClassCastException is thrown. The size of the
     * array is not checked for consistency with the code-block dimensions.
     *
     * @param arr The data array to use. Must be a float array.
     * */
    public final void setData(Object arr) {
        data = (float[]) arr;
    }

    /**
     * Sets the data array to the specified one. The size of the array is not
     * checked for consistency with the code-block dimensions. This method is
     * more efficient than 'setData()'.
     *
     * @param arr The data array to use.
     * */
    public final void setDataFloat(float[] arr) {
        data = arr;
    }
}
