/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.wavelet.analysis;

import jj2000.j2k.image.*;
import jj2000.j2k.wavelet.*;

/**
 * This interface extends the ImgData interface with methods that are
 * necessary for forward wavelet data (i.e. data that is produced by a forward
 * wavelet transform).  */
public interface ForwWTDataProps extends ImgData {

    /**
     * Returns the reversibility of the given tile-component. Data is
     * reversible when it is suitable for lossless and lossy-to-lossless
     * compression.
     *
     * @param t Tile index
     *
     * @param c Component index
     *
     * @return true is the data is reversible, false if not.
     * */
    public boolean isReversible(int t,int c);

    /**
     * Returns a reference to the root of subband tree structure representing
     * the subband decomposition for the specified tile-component.
     *
     * @param t The index of the tile.
     *
     * @param c The index of the component.
     *
     * @return The root of the subband tree structure, see Subband.
     *
     * @see SubbandAn
     *
     * @see Subband
     * */
    public SubbandAn getAnSubbandTree(int t,int c);

    /**
     * Returns the horizontal offset of the code-block partition. Allowable
     * values are 0 and 1, nothing else.
     * */
    public int getCbULX();

    /**
     * Returns the vertical offset of the code-block partition. Allowable
     * values are 0 and 1, nothing else.
     * */
    public int getCbULY();
}
