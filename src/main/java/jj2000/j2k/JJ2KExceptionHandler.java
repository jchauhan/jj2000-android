/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package jj2000.j2k;

/**
 * This class handles exceptions. It should be used in places where it
 * is not known how to handle the exception, and the exception can not
 * be thrown higher in the stack.
 *
 * <P>Different options can be registered for each Thread and
 * ThreadGroup. <i>This feature is not implemented yet</i>
 *
 */
public class JJ2KExceptionHandler {

    /**
     * Handles the exception. If no special action is registered for
     * the current thread, then the Exception's stack trace and a
     * descriptive message are printed to standard error and the
     * current thread is stopped.
     *
     * <P><i>Registration of special actions is not implemented yet.</i>
     *
     * @param e The exception to handle
     *
     *
     * */
    public static void handleException(Throwable e) {
        // Test if there is an special action (not implemented yet)
        
        // If no special action
        
        // Print the Exception message and stack to standard error
        // including this method in the stack.
        e.fillInStackTrace();
        e.printStackTrace();
        // Print an explicative message
        System.err.println("The Thread is being terminated bacause an " +
                           "Exception (shown above)\n" +
                           "has been thrown and no special action was " +
                           "defined for this Thread.");
        // Stop the thread (do not use stop, since it's deprecated in
        // Java 1.2)
        throw new ThreadDeath();
    }
}
