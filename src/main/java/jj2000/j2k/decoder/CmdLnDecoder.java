/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package jj2000.j2k.decoder;

import jj2000.j2k.util.*;

import java.io.*;

/**
 * This class runs the JJ2000 decoder from the command line interface. It
 * parses command-line arguments in order to fill a ParameterList object. Then
 * this one is provided to a Decoder object.
 * */
public class CmdLnDecoder{

    /** The parameter list (with modules arguments) */
    private ParameterList pl;

    /** The default parameter list (with modules arguments) */
    private ParameterList defpl;

    /** The current Decoder object */
    private Decoder dec;

    /**
     * The starting point of the program. It calls the constructor with the
     * command line options in a String array.
     *
     * @param argv The command line parameters
     * */
    public static void main(String argv[]) {
        if (argv.length == 0) {
            FacilityManager.getMsgLogger()
                .println("CmdLnDecoder: JJ2000's JPEG 2000 Decoder\n"+
                         "    use jj2000.j2k.decoder.CmdLnDecoder -u "+
                         "to get help\n",0,0);
            System.exit(1);
        }

        new CmdLnDecoder(argv);
    }

    /**
     * Instantiates a command line decoder object, width the 'argv' command
     * line arguments. It also initializes the default parameters. If the
     * argument list is empty an IllegalArgumentException is thrown. If an
     * error occurs while parsing the arguments error messages are written to
     * stderr and the run exit code is set to non-zero, see getExitCode()
     *
     * @exception IllegalArgumentException If 'argv' is empty
     *
     * @see Decoder#getExitCode
     * */
    public CmdLnDecoder(String argv[]) {
        // Initialize default parameters
        defpl = new ParameterList();
	String[][] param = Decoder.getAllParameters();

	for (int i=param.length-1; i>=0; i--) {
	    if(param[i][3]!=null)
		defpl.put(param[i][0],param[i][3]);
        }

        // Create parameter list using defaults
        pl = new ParameterList(defpl);

        if (argv.length == 0 ) {
            throw new IllegalArgumentException("No arguments!");
        }

        // Parse arguments from argv
        try {
            pl.parseArgs(argv);
        }
        catch (StringFormatException e) {
            System.err.println("An error occured while parsing the "+
			       "arguments:\n"+e.getMessage());
            return;
        }
        // Parse the arguments from some file?
        if (pl.getParameter("pfile") != null) {
            // Load parameters from file
            InputStream is;
            try {
                is = new FileInputStream(pl.getParameter("pfile"));
                is = new BufferedInputStream(is);
                pl.load(is);
            }
            catch (FileNotFoundException e) {
                System.err.println("Could not load the argument file " +
				   pl.getParameter("pfile"));
                return;
            }
            catch (IOException e) {
                System.err.println("An error ocurred while reading from "+
				   "the argument "+"file "+ 
				   pl.getParameter("pfile"));
                return;
            }
            try {
                is.close();
            }
            catch (IOException e) {
                System.out.println("[WARNING]: Could not close the argument"+
				   " file after reading");
            }
            // Now reparse command line arguments so that they override file
            // arguments
            try {
                pl.parseArgs(argv);
            }
            catch (StringFormatException e) {
                System.err.println("An error occured while re-parsing the "+
				   "arguments:\n"+e.getMessage());
                return;
            }
        }

	// Instantiate the Decoder object
        dec = new Decoder(pl);
        if (dec.getExitCode() != 0) { // An error ocurred
            System.exit(dec.getExitCode());
        }

        // Run the decoder
        try {
            dec.run();
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
        finally {
            if (dec.getExitCode() != 0) {
                System.exit(dec.getExitCode());
            }
        }
    }
}
